#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use battlesnake::moves::Movement;
use battlesnake::payloads::{InitPayload, Head, Tail};
use battlesnake::requests::Turn;
use rocket_contrib::json::Json;

mod game;
use game::Game;

#[get("/")]
fn index() -> Json<InitPayload> {
    Json(InitPayload::new()
        .with_author("Ian S. Pringle")
        .with_color("#444444")
        .with_head(Head::ShacTigerKing)
        .with_tail(Tail::Hook)
        .with_version("0.1.0"))
}

#[post("/start")]
fn start() -> &'static str {
    "hi there"
}

#[post("/move", format = "json", data = "<turn>")]
fn action(turn: Json<Turn>) -> Json<Movement> {
    //println!("{:#?}", &turn);
    let game: Game = Game{ turn: turn.into_inner() };
    let movement = game.run();
    Json(movement)
}

#[post("/end")]
fn end() -> &'static str {
    "good-bye"
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, start, action, end])
        .launch();
}
