use battlesnake::moves::{Direction, Movement};
use battlesnake::requests::{Location, Turn};
use rand;
use rand::seq::SliceRandom;
use std::iter::FromIterator;

#[derive(Clone, Debug, Default)]
pub struct Game {
    pub turn: Turn,
}

impl Game {
    fn circle(self) -> Direction {
        match self.last_direction() {
            Direction::Up => Direction::Right,
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
        }
    }

    fn distance_to(self, target: Location) -> i32 {
        let curr: Location = self.turn.you.head;
        (curr.x - target.x).abs() + (curr.y - target.y).abs()
    }

    fn get_boundaries(self) -> Vec<Location> {
        let height = self.turn.board.height - 1;
        let width = self.turn.board.width - 1;
        let mut outside = Vec::new();
        for x in -1..width+2 {
            for y in -1..height+2 {
                if x < 0 || x > width || y < 0 || y > height {
                    let loc = Location{x: x, y: y};
                    //println!("{:#?}", &loc);
                    outside.push(loc);
                }
            }
        }
        outside
    }

    fn get_dangers(self, food_safe: bool) -> Vec<Location> {
        // Other snakes are always dangerous
        let mut curr_dangers: Vec<Location> = self.turn.board.snakes
            .iter()
            .flat_map(|x| x.body.clone())
            .collect();
        // Hazards are always dangerous
        curr_dangers.append(&mut self.turn.board.hazards.clone());
        // Your body is always dangerous
        curr_dangers.append(&mut self.turn.you.body.clone());
        // Outside the board is always dangerous
        curr_dangers.append(&mut self.clone().get_boundaries());
        // Food is only dangerous if we don't want to eat
        if !food_safe {
            curr_dangers.append(&mut self.turn.board.food.clone());
        }
        curr_dangers
    }

    fn get_food(self) -> Vec<Location> {
        self.turn.board.food.clone()
    }

    fn get_hazards(self) -> Vec<Location> {
        self.turn.board.hazards.clone()
    }

    fn get_others(self) -> Vec<Location> {
        let others: Vec<Location> = self.turn.board.snakes
            .iter()
            .flat_map(|x| x.body.clone())
            .collect();
        others
    }

    fn get_self(self) -> Vec<Location> {
        self.turn.you.body.clone()
    }

    fn go_towards(self, target: Location) -> Direction {
        let towards: (i32, i32) = (
            go_towards_helper(self.turn.you.head.x.clone() - target.x),
            go_towards_helper(self.turn.you.head.y.clone() - target.y)
        );

        // 0th is up
        // 1st is right
        // 2nd is down
        // 3rd is left
        let safety: [bool; 4] = [
            self.clone().is_safe(Direction::Up, true),
            self.clone().is_safe(Direction::Right, true),
            self.clone().is_safe(Direction::Down, true),
            self.clone().is_safe(Direction::Left, true),
        ];

        // -1 means go right/up
        //  0 means on the correct col/row for that axis
        //  1 means go left/down
        match towards {
            (-1,  -1) => {
                if safety[0] { Direction::Up }
                else if safety[1] { Direction::Right }
                else if safety[2] { Direction::Down }
                else { Direction::Left }
            },
            (-1,   0) => {
                if safety[1] { Direction::Right }
                else if safety[0] { Direction::Up }
                else if safety[2] { Direction::Down }
                else { Direction::Left }
            },
            (-1,   1) => {
                if safety[1] { Direction::Right }
                else if safety[2] { Direction::Down }
                else if safety[0] { Direction::Up }
                else { Direction::Left }
            },
            ( 0,  -1) => {
                if safety[2] { Direction::Down }
                else if safety[1] { Direction::Right }
                else if safety[3] { Direction::Left }
                else { Direction::Up }
            },
            ( 0,   0) => {
                if safety[0] { Direction::Up }
                else if safety[1] { Direction::Right }
                else if safety[2] { Direction::Down }
                else { Direction::Left }
            },
            ( 0,   1) => {
                if safety[2] { Direction::Down }
                else if safety[1] { Direction::Right }
                else if safety[3] { Direction::Left }
                else { Direction::Up }
            },
            ( 1,  -1) => {
                if safety[0] { Direction::Up }
                else if safety[3] { Direction::Left }
                else if safety[2] { Direction::Down }
                else { Direction::Right }
            },
            ( 1,   0) => {
                if safety[3] { Direction::Left }
                else if safety[0] { Direction::Up }
                else if safety[2] { Direction::Down }
                else { Direction::Right }
            },
            ( 1,   1) => {
                if safety[2] { Direction::Down }
                else if safety[3] { Direction::Left }
                else if safety[2] { Direction::Down }
                else { Direction::Right }
            },
            _ => { Direction::Up } // This should never happen
        }
    }

    fn is_safe(self, new_direction: Direction, food_safe: bool) -> bool {
        let curr_head = self.turn.you.body.first().clone().unwrap();
        let curr_dangers = self.clone().get_dangers(food_safe);
        let new_head: Location = match new_direction {
            Direction::Up => Location {
                x: curr_head.x,
                y: curr_head.y + 1,
            },
            Direction::Down => Location {
                x: curr_head.x,
                y: curr_head.y - 1,
            },
            Direction::Left => Location {
                x: curr_head.x - 1,
                y: curr_head.y,
            },
            Direction::Right => Location {
                x: curr_head.x + 1,
                y: curr_head.y,
            }
        };
        if curr_dangers.iter().any(|i| i == &new_head) {
            false
        } else {
            true
        }
    }

    fn last_direction(&self) -> Direction {
        let head_loc = self.turn.you.body.first().unwrap();
        let next_loc = self.turn.you.body.get(1).unwrap();
        let diff: (i32, i32) = (head_loc.x - next_loc.x, head_loc.y - next_loc.y);
        match diff {
            ( 1,  0) => Direction::Right,
            (-1,  0) => Direction::Left,
            ( 0,  1) => Direction::Up,
            ( 0, -1) => Direction::Down,
            _ => Direction::Up, // Only only reach this on move 0
        }
    }

    fn nearest(self, items: Vec<Location>) -> Location {
        let mut nearest: (i32, Location) = (i32::MAX, Location::default());
        for item in items {
            let distance = self.clone().distance_to(item);
            if distance < nearest.0 {
                nearest.0 = distance;
                nearest.1 = item;
            }
        }
        nearest.1
    }

    pub fn new() -> Game { Default::default() }

    fn random_move(self) -> Direction {
        let mut rng = rand::thread_rng();
        let mut directions = [Direction::Up,
                          Direction::Down,
                          Direction::Left,
                          Direction::Right];
        directions.shuffle(&mut rng);
        for direction in directions.iter() {
            if self.clone().is_safe(direction.clone(), true) {
                return direction.clone()
            }
        }
        Direction::Up
    }

    pub fn run(self) -> Movement {
        self.clone().visualize();
        let foods = self.clone().get_food();
        let nearest_food = self.clone().nearest(foods);
        let movement: Direction;
        if self.turn.you.health.clone() > self.clone().distance_to(nearest_food) {
            movement = self.clone().go_towards(*self.turn.you.body.last().unwrap())
        } else {
            movement = self.go_towards(nearest_food);
        }
        Movement::new(movement)
    }

    fn visualize(self) {
        let length = self.turn.board.height.clone() as usize + 2;
        let width = self.turn.board.width.clone() as usize + 2;
        let mut board: Vec<Vec<&str>> = vec![vec!["."; width]; length];

        let edges = self.clone().get_boundaries();
        for loc in edges {
            let x = (loc.x + 1) as usize;
            let y = (loc.y + 1)as usize;
            board[y][x] = "X";
        }

        let foods = self.clone().get_food();
        for loc in foods {
            let x = (loc.x + 1) as usize;
            let y = (loc.y + 1)as usize;
            board[y][x] = "O";
        }

        let hazards = self.clone().get_hazards();
        for loc in hazards {
            let x = (loc.x + 1) as usize;
            let y = (loc.y + 1)as usize;
            board[y][x] = "O";
        }

        let others = self.clone().get_others();
        for loc in others {
            let x = (loc.x + 1) as usize;
            let y = (loc.y + 1)as usize;
            board[y][x] = "S";
        }

        let you = self.clone().get_self();
        let (head, body) = you.split_first().unwrap();
        board[head.y as usize + 1][head.x as usize + 1] = "H";
        for loc in body {
            let x = (loc.x + 1) as usize;
            let y = (loc.y + 1)as usize;
            board[y][x] = "B";
        }

        let mut s_board = String::new();
        for row in board.iter().rev() {
            let mut s_row: String = String::from_iter(row.clone());
            s_row.push_str("\n");
            s_board.push_str(&s_row);
        }
        println!("{}", s_board);
    }
}

fn go_towards_helper(x: i32) -> i32 {
    match x {
        x if x == 0 => 0,
        x if x.is_positive() => 1,
        x if x.is_negative() => -1,
        _ => 0, // This should never happen
    }
}

