FROM rustlang/rust:nightly as builder
WORKDIR /usr/src
RUN rustup target add x86_64-unknown-linux-musl
RUN USER=root cargo new aerugo
WORKDIR /usr/src/aerugo
COPY Cargo.toml ./
RUN cargo build --release
COPY src ./src
RUN cargo install --target x86_64-unknown-linux-musl --path .

FROM scratch
COPY --from=builder /usr/local/cargo/bin/aerugo .
USER 1000
EXPOSE 8000
CMD ["./aerugo"]
