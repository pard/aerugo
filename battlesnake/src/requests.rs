use serde::Deserialize;

#[derive(Clone, Copy, Debug, Default, Deserialize, Eq, PartialEq)]
pub struct Location {
    pub x: i32,
    pub y: i32,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
pub struct Turn {
    pub game: Game,
    pub turn: i32,
    pub board: Board,
    pub you: Snake,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
pub struct Game {
    pub id: String,
    pub timeout: i32,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
pub struct Board {
    pub height: i32,
    pub width: i32,
    pub food: Vec<Location>,
    pub hazards: Vec<Location>,
    pub snakes: Vec<Snake>,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
#[serde(default)]
pub struct Snake {
    pub id: String,
    pub name: String,
    pub health: i32,
    pub body: Vec<Location>,
    pub latency: String,
    pub head: Location,
    pub length: i32,
    pub shout: String,
    pub squad: String,
}
