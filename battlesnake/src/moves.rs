use serde::Serialize;

#[derive(Clone, Copy, Debug, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Direction {
    Up,
    Down,
    Left,
    Right
}


#[derive(Debug, Serialize)]
pub struct Movement {
    #[serde(rename = "move")]
    pub movement: Direction,
}

impl Movement {
    pub const UP: Movement = Movement { movement: Direction::Up };
    pub const DOWN: Movement = Movement { movement: Direction::Down };
    pub const LEFT: Movement = Movement { movement: Direction::Left };
    pub const RIGHT: Movement = Movement { movement: Direction::Right };

    pub fn new(direction: Direction) -> Movement {
        Movement { movement: direction }
    }

}
