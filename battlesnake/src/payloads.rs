use serde::Serialize;

#[derive(Debug, Serialize)]
pub enum Apiversion {
    #[serde(rename = "1")]
    ONE,
}

impl Default for Apiversion {
    fn default() -> Self { Apiversion::ONE }
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum Head {
    Default,
    Beluga,
    Bendr,
    Dead,
    Evil,
    Fang,
    Pixel,
    SandWorm,
    Safe,
    Shades,
    Silly,
    Smile,
    Tongue,
    BwcBonhomme,
    BwcEarmuffs,
    BwcRudolph,
    BwcScarf,
    BwcSki,
    BwcSnowWorm,
    BwcSnowman,
    ShacCaffeine,
    ShacGamer,
    ShacWorkout,
    ShacTigerKing,
}

impl Default for Head {
    fn default() -> Self { Head::Default }
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum Tail {
    Default,
    BlockBum,
    Bolt,
    Curled,
    FatRattle,
    Freckle,
    Hook,
    Pixel,
    RoundBum,
    Sharp,
    Skinny,
    SmallRattle,
    BwcSki,
    BwcSnowWorm,
    BwcSnowman,
    ShacCoffee,
    ShacMouse,
    ShacWeight,
    ShacTigerTail,
}

impl Default for Tail {
    fn default() -> Self { Tail::Default }
}

#[derive(Debug, Default, Serialize)]
pub struct InitPayload {
    pub apiversion: Apiversion,
    pub author: String,
    pub color: String,
    pub head: Head,
    pub tail: Tail,
    pub version: String,
}

impl InitPayload {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn with_author(mut self, author: &str) -> Self {
        self.author = author.to_owned();
        self
    }

    pub fn with_color(mut self, color: &str) -> Self {
        self.color = color.to_owned();
        self
    }

    pub fn with_head(mut self, head: Head) -> Self {
        self.head = head;
        self
    }

    pub fn with_tail(mut self, tail: Tail) -> Self {
        self.tail = tail;
        self
    }

    pub fn with_version(mut self, version: &str) -> Self {
        self.version = version.to_owned();
        self
    }
}
